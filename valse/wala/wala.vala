using GLib.FileUtils;
using Gee;

namespace Valse {

    /**
    * Default render engine for Valse.
    */
    public class Wala : Object, RenderEngine {

        /**
        * Name of the render engine.
        */
        public string name {get; set; default = "Wala"; }

        /**
        * The model keyword.
        */
        private string model_keyword {get; set; default = "mod";}

        private static HashMap<string, WalaFilter> filters {get; set; default = new HashMap<string, WalaFilter> ();}

        /**
        * Wala constructor's. It adds the default filters.
        */
        public Wala () {

            add_filter ("upper", (ref input, args) => {
                return input.up ();
            });
            add_filter ("lower", (ref input, args) => {
                return input.down ();
            });
            add_filter ("capitalize", (ref input, args) => {
                string res = "";
                for (int i = 0; i < input.length; i++) {
            		if (input.valid_char (i)) {
                        if (i == 0) {
                            res += input.get_char (i).to_string ().up ();
                        } else {
                            res += input.get_char (i).to_string ();
                        }
            		}
            	}
                return res;
            });
            add_filter ("safe", (ref input, args) => {
                string res = input.replace ("&amp", "&");
                res = res.replace ("&quot", "\"");
                res = res.replace ("&#039", "'");
                res = res.replace ("&lt", "<");
                res = res.replace ("&gt", ">");
                return res;
            });
            add_filter ("cut", (ref input, args) => {
                if (args.length > 1) {
                    string res = input;
                    foreach (string arg in args) {
                        res = res.replace (arg, "");
                    }
                    return res;
                } else {
                    return input.replace (args[0], "");
                }
            });
            add_filter ("default", (ref input, args) => {
                if (input.length <= 0) {
                    return args[0];
                } else {
                    return input;
                }
            });
            add_filter ("linebreaks", (ref input, args) => {
                return input.replace ("\n", "<br />");
            });
            add_filter ("pluralize", (ref input, args) => {
                if (int.parse (input) > 1) {
                    if (args.length == 0) {
                        return "s";
                    } else {
                        return args[0];
                    }
                } else {
                    return "";
                }
            });
            add_filter ("truncate", (ref input, args) => {
                string striped = input.strip ();
                int limit = int.parse (args[0]);
                if (striped.length > limit) {
                    return striped.slice (0, limit-1);
                } else {
                    return striped;
                }
            });
            add_filter ("strip", (ref input, args) => {
                return input.strip ();
            });
        }

        /**
        * Renders a page.
        *
        * @param view The {@link Valse.View} to render.
        */
        public string render_page (View view) {

            try {
                string result = view.content;
                result = result.replace ("{{ render_engine }}", this.name);

                // Interprétation des variables du modèle

                Regex var_re = new Regex ("{{ *%s\\.(?P<prop>([A-z]|[0-9]|)*)(\\|)?(?<filter>[[:graph:]]*)(?<params>( +[[:graph:]]*)*?) *}}".printf (this.model_keyword));
                MatchInfo var_info;
                bool go_var = var_re.match (result, 0, out var_info);

                while (go_var) {
                    string prop_name = var_info.fetch_named ("prop");
                    string prop_val = view.model [prop_name];
                    string? var_filters = var_info.fetch_named ("filter");
                    string? params = var_info.fetch_named ("params");

                    // Echappement des caractères HTML

                    prop_val = prop_val.replace ("&", "&amp");
                    prop_val = prop_val.replace ("\"", "&quot");
                    prop_val = prop_val.replace ("'", "&#039");
                    prop_val = prop_val.replace ("<", "&lt");
                    prop_val = prop_val.replace (">", "&gt");

                    // si il y a un filtre on l'applique
                    if (var_filters.length > 0) {
                        foreach (string filter in var_filters.split ("|")) {
                            assert (filters [filter] != null);
                            string[] filter_args = {};
                            if (params.length > 0) {
                                foreach (string param in params.split (" :")) {
                                    if (param.length > 0) {
                                        filter_args += param.strip ();
                                    }
                                }
                            }
                            prop_val = filters [filter].cb (ref prop_val, filter_args);
                        }
                    }

                    result = var_re.replace_eval (result, result.length, 0, 0, (info, res) => {
                        res.append (prop_val);
                        return true;
                    });

                    go_var = var_info.next ();
                }

                Regex include_re = new Regex ("{% *include '(?P<file>.*)' *%}");
                MatchInfo inc_info;
                bool go_inc = include_re.match (result, 0, out inc_info);

                while (go_inc) {

                    string file_name = inc_info.fetch_named ("file");
                    string file_content = "";
                    get_contents (file_name, out file_content);

                    View included = new View ();
                    included.content = file_content;
                    included.model = view.model;

                    file_content = this.render_page (included);

                    result = include_re.replace_eval (result, result.length, 0, 0, (info, res) => {
                        res.append (file_content);
                        return true;
                    });

                    go_inc = inc_info.next ();
                }

                Regex tag_re = new Regex ("{% *tag '(?P<cond>.*)' *%}(?P<content>.*){% *endtag *%}");
                MatchInfo tag_info;
                bool tag_go = tag_re.match (result, 0, out tag_info);

                while (tag_go) {
                    foreach (string tag in view.tags) {
                        if (tag_info.fetch_named ("cond") == tag) {
                            result = tag_re.replace (result, result.length, 0, tag_info.fetch_named ("content"));
                        }
                        tag_go = tag_info.next ();
                    }
                }

                return result;
            } catch {
                return Errors.server_error ("Error while rendering page using Wala.").body;
            }
        }

        /**
        * Registers a new filter.
        *
        * @param name The name of the new filter.
        * @param cb The callback for this filter.
        */
        public static void add_filter (string name, FilterCallback cb) {
            filters [name] = new WalaFilter (cb);
        }

    }
}
