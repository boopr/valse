using Gee;
using Sqlite;

namespace Valse {

    /**
    * The base of any controller.
    * Provides methods like {@link Valse.Controller.page} or
    * {@link Valse.Controller.db}.
    */
    public class Controller : Object {

        /**
        * The actions registered for this controller
        */
        public HashMap<string, Action> actions {get; set; default = new HashMap<string, Action> ();}

        /**
        * Renders a page using the default render engine.
        *
        * @param file The path of the file containing the view to render
        * @param obj_model The model for the view that will be rendered
        * @param tags The tags for this view
        * @return A {@link Valse.Response}, ready to be sent
        */
        public Response page (string file, Object obj_model = new Object (), string[] tags = new string[] {}) {
            string html; // Le contenu de file
            html = get_contents (file); // on lit le fichier

            var model = create_model(obj_model); // on transforme obj_model en HashMap pour l'utiliser dans la vue

            // on crée la vue
            View view = new View ();
            view.content = html;
            view.model = model;
            view.tags = tags;

            // On renvoie l'interprétation de la vue
            return new Response.from_html (Router.options.render_engine.render_page (view));
        }

        /**
        * Shortcut to access the {@link Valse.DataBase} class.
        */
        public DataBase db {
            owned get {
                return new DataBase ();
            }
        }

        /**
        * Reads the content of a file.
        *
        * @param path The path of the file to read
        * @return The file's content
        */
        private string get_contents (string path) {
            // Le résultat final
            string result = "";

            // On crée un fichier correspondant à path
            var file = File.new_for_path (path);
            // On vérifie qu'il existe
            if (!file.query_exists ()) {
                // si il n'existe pas, on envoie une erreur
                return Errors.not_found (path).body;
            }

            // on lit le fichier ligne par ligne
            try {
                var dis = new DataInputStream (file.read ());
                string line;
                while ((line = dis.read_line (null)) != null) {
                    result += line + "\n";
                }
            } catch (Error e) {
                error ("%s", e.message);
            }

            return result;
        }

        /**
        * Generate a model that could be used in a view
        *
        * @param obj The object that stands for the model
        * @return A {@link Gee.HashMap} usable in a view
        */
        private HashMap<string, string> create_model (Object obj) {

            HashMap<string, string> result = new HashMap<string, string> ();

            foreach (ParamSpec spec in obj.get_class ().list_properties ()) {
                // pour chaque propriété ...
                GLib.Value val = GLib.Value (typeof (string)); // pour récupérer la valeur de la variable
                obj.get_property (spec.get_nick (), ref val); // on récupère cette valeur
                result [spec.get_nick ()] = (string) val; // on la défini dans notre modèle (utilisé dans la vue)
            }

            return result;
        }

        /**
        * Registers a simple action.
        *
        * @param name The name of the action, his route.
        * @param act The handler for this action.
        */
        public void add_action (string name, ActionHandler act) {
            actions[name] = new Action (act);
        }

        /**
        * Registers a complex action.
        *
        * @param name The name of the action, his route.
        * @param act The handler for this action.
        */
        public void add_complete_action (string name, CompleteActionHandler act) {
            actions[name] = new Action.complete (act);
        }

        /**
        * Run an action of this controller.
        *
        * @param name The name of the action to run.
        * @param msg The {@link Soup.Message} to give to the action, if it's complex.
        * @param query The {@link Gee.HashTable} to give to the action, if it's complex.
        * @param client The {@link Soup.ClientContext} to give to the action, if it's complex.
        */
        public Response run_action (string name, Soup.Message msg, HashTable<string, string>? query, Soup.ClientContext client, string? id = null) {
            // on cherche l'action appellée name.

            if (this.actions.has_key (name)) {
                if (this.actions [name].handler != null) {
                    // si c'est une action simple ...
                    return this.actions [name].handler ();
                } else if (this.actions [name].complete_handler != null) {
                    // si c'est une action complexe
                    return this.actions [name].complete_handler (new Request (msg, query, client, id), new Response (msg));
                }
            }
            // Si on n'en trouve pas, on envoie une erreur
            return Errors.not_found (name);
        }

    }

}
