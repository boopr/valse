using Soup;
using Gee;
using Sqlite;

namespace Valse {

    /**
    * Base class of the server.
    *
    * It lets you register controllers, mime-types ...
    */
    public class Router : Object {

        /**
        * The {@link Soup.Server} that is the base of this.
        */
        private Server server {get; set;}

        /**
        * The routes and the associed controllers.
        */
        private HashMap<string, Controller> routes {get; set;}

        /**
        * The {@link Valse.RouterOptions} of this.
        */
        public static RouterOptions options {get; set; default = new RouterOptions ();}

        /**
        * Les différents mime-types connus du serveur.
        */
        public HashMap<string, string> mime_types {get; set; default = new HashMap<string, string> ();}

        private string[] static_files {get; set;}

        /**
        * Constructor of the server. Initialize the internal {@link Soup.Server} and registers some commmons mime-types.
        *
        * @param port The port on which to listen.
        */
        public Router (int port = 88) {
            this.routes = new HashMap<string, Controller> ();
            this.register_mime ("css",  "text/css");
            this.register_mime ("html", "text/html");
            this.register_mime ("png",  "image/png");
            this.register_mime ("gif",  "image/gif");
            this.register_mime ("jpg",  "image/jpeg");
            this.register_mime ("jpeg", "image/jpeg");
            this.register_mime ("ico",  "image/x-icon");
            this.register_mime ("xml",  "application/xml");
            this.register_mime ("json", "application/json");
            this.register_mime ("js",   "application/javascript");

            this.server = new Server(Soup.SERVER_PORT, port);
            this.server.add_handler (null, soup_handler);
            this.static_files = this.get_static_files_list ();
        }

        /**
        * Gets all the file of the folder
        *
        * @param static_path The folder to look up
        * @return The path list of the folder files
        */
        private string[] get_static_files_list () {
            string[] res = {};
            try {
                var lib_path = File.new_for_path ("static/");
                var enumerator = lib_path.enumerate_children (FileAttribute.STANDARD_NAME, 0, null);
                var file_info = enumerator.next_file (null);

                while (file_info != null) {
                   res += file_info.get_name ();
                   file_info = enumerator.next_file (null);
                }
            } catch (Error err) {
                print ("[STATIC] The \"static/\" folder doesn't exist or it's empty\n");
            }
            return res;
        }

        /**
        * Registers a new mime-type.
        *
        * @param ext The file extension
        * @param mime The associed
        */
        public void register_mime (string ext, string mime) {
            this.mime_types [ext] = mime;
        }

        /**
        * Retrives the mime-type of for a given file
        *
        * @param path Le fichier dont on veux connaître le mime-type
        * @return Le mime-type du fichier
        */
        private string get_mime (string path) {

            string[] splitted_path = path.split (".");
            string ext = splitted_path [splitted_path.length - 1]; // onrécupère juste la dernière partie du chemin

            string result = "text/plain";

            if (this.mime_types [ext] != null) {
                result = this.mime_types [ext];
            }

            return result;
        }

        /**
        * Split a path
        *
        * @param path Le chemin à séparer
        * @return Un tableau contenant les différents morceaux du chemin
        */
        private string[] split_path (string path) {
            string[] result = new string[] {};
            foreach (string part in path.split ("/")) {
                if (part.length != 0) {
                    // on ajoute au résultat que si  c'est pas vide
                    result += part;
                }
            }

            return result;
        }

        /**
        * The handler for each request entering on the Soup server.
        *
        * Choose which file or action to send.
        */
        private void soup_handler (Soup.Server server, Soup.Message msg, string path, GLib.HashTable<string, string>? query, Soup.ClientContext client) {

            string debug_message = "";

            if (DEBUG) {
                DateTime now = new DateTime.now_local ();
                debug_message = "[%s] Request on \"%s\" -> ".printf (now.format (DEBUG_DATE_FORMAT), path);
            }


            uint8[] response = "<h1>You found a bug in Valse</h1>".data;
            string mime = "text/plain";
            HashMap<string, string> hdrs = new HashMap<string, string> ();

            string[] clean_path = split_path (path);
            bool is_static = false; 

            if (clean_path.length > 0 && clean_path [clean_path.length - 1] in this.static_files) {
                try {
                    GLib.FileUtils.get_data ("static/" + clean_path [clean_path.length - 1], out response);
                    msg.set_status (200);
                } catch (Error err) {
                    print ("[ERROR] \"static/%s\" not found.\n", clean_path [clean_path.length - 1]);
                    msg.set_status (404);
                }

                is_static = true;
                mime = get_mime (path);
                Soup.Date expiration = new Soup.Date.from_now (0);
                expiration.year += 1;
                hdrs ["Expires"] = expiration.to_string (DateFormat.HTTP);
                hdrs ["Last-Modified"] = new Soup.Date.from_now (-10).to_string (DateFormat.HTTP);
            } else {
                Response res = new Response (msg);

                switch (clean_path.length) {
                    case 0:
                        // On prends l'index de la home
                        res = this.routes["home"].run_action ("index", msg, query, client);
                        break;
                    case 1:
                        if (this.routes [clean_path [0]] != null) {
                            // On regarde si le chemin existe dans les controlleurs
                            res = this.routes [clean_path [0]].run_action ("index", msg, query, client);
                        } else if (this.routes ["home"].actions [clean_path [0]] != null) {
                            // Sinon on regarde dans les actions de la home
                            res = this.routes ["home"].run_action (clean_path [0], msg, query, client);
                        } else {
                            // on prends l'index de la home, avec un ID
                            res = Errors.not_found (path);
                        }
                        break;
                    case 2:
                        Controller ctrl = this.routes[clean_path [0]];

                        if (ctrl != null && ctrl.actions [clean_path [1]] != null) {
                            // on regarde dans les actions du controlleur
                            res = ctrl.run_action (clean_path [1], msg, query, client);
                        } else if (this.routes ["home"].actions [clean_path [0]] != null) {
                            // On regarde dans les actions de la home avec un ID
                            res = this.routes ["home"].run_action (clean_path [0], msg, query, client, clean_path[1]);
                        } else {
                            // on prend l'index du controlleur avec un ID
                            res = Errors.not_found (path);
                        }
                        break;
                    case 3:
                        // on prends le controlleur, l'action et l'iD spécifié
                        res = this.routes [clean_path [0]].run_action (clean_path [1], msg, query, client, clean_path [2]);
                        break;
                }

                if (DEBUG) {
                    debug_message += "%s %s %d\n".printf (msg.method, res.mime_type, res.error_code);
                }

                hdrs = res.headers;

                msg.set_status (res.error_code);
                response = res.body.data;
                mime = res.mime_type;
            }

            // on définit les headers
            foreach (var entry in hdrs.entries) {
                msg.response_headers.append (entry.key, entry.value);
            }

            if (DEBUG && is_static) {
                debug_message += "%s %s %u\n".printf (msg.method, mime, msg.status_code);
            }

            msg.set_response (mime, Soup.MemoryUse.COPY, response);

            if (DEBUG) {
                print (debug_message);
            }
        }

        /**
        * Registers a new controller.
        *
        * @param route The route of the controller
        * @param controller The controller associed with this route.
        */
        public void register (string route, Controller controller) {
            if (!this.routes.has_key (route)) {
                this.routes.set (route, controller);
            } else {
                print ("ERROR: \"/%s\" has already a controller.\n", route);
                return;
            }
        }

        /**
        * Run the server.
        */
        public void listen () {
            if (DEBUG) {
                DateTime now = new DateTime.now_local ();
                print ("%s\n", now.format (DEBUG_DATE_FORMAT));
                print ("%s version %s\n", NAME, VERSION);
                print ("Development server available at http://127.0.0.1:%u\n", this.server.port);
                print ("%d static files found\n", this.static_files.length);
                print ("Quit the server with CTRL-C\n");
            }

            this.server.run ();
        }
    }

}
