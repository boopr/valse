namespace Valse {

    /**
    * Delegate to stock a simple action.
    */
    public delegate Response ActionHandler();

    /**
    * Delegate to stock a complex action.
    */
    public delegate Response CompleteActionHandler (Request req, Response res);

    /**
    * Handles an action.
    */
    public class Action : Object {

        /**
        * Handler for a simple action.
        */
        public ActionHandler handler {get; set;}

        /**
        * Handler for a complex action.
        */
        public CompleteActionHandler complete_handler {get; set;}

        /**
        * Create a new simple action, with the given handler.
        *
        * @param hdlr The handler for the new action.
        */
        public Action (ActionHandler hdlr) {
            this.handler = hdlr;
        }

        /**
        * Create a new complex action, with the given handler.
        *
        * @param hdlr The handler for the new action.
        */
        public Action.complete (CompleteActionHandler hdlr) {
            this.complete_handler = hdlr;
        }

    }

}
