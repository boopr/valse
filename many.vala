namespace Valse {

    /**
    * Used to have collections in
    */
    public class Many<G> : Object {

        public Many<G> () {
            this._arr = new G[] {};
        }

        private G[] _arr;

        public void add (G item) {
            _arr += item;
        }

        public G get (int index) {
            return _arr[index];
        }

        /**
        * Transforms a {@link Valse.Many} into a {@link GLib.Value} array.
        */
        public Value[] to_val_arr () {
            Value[] res = new Value[] {};
            // we transform each element into a Value
            foreach (G item in _arr) {
                if (typeof (G) == typeof (string)) {
                    // G is a string
                    Value val = Value (typeof (string));
                } else if (typeof (G) == typeof (int)) {
                    // G is an int
                    Value val = Value (typeof (int));
                } else if (typeof (G) == typeof (float)) {
                    // G is a float
                    Value val = Value (typeof (float));
                } else if (typeof (G) == typeof (double)) {
                    // G is a double
                    Value val = Value (typeof (double));
                } else {
                    // G is probably an object
                }
            }
            return res;
        }

    }

}
