try {
    rm serv.exe -ErrorAction Stop
}
catch {
    echo "serv.exe n'existe pas, impossible de le supprimer."
}

valac --pkg libsoup-2.4 --pkg gee-1.0 --pkg sqlite3 valse/*.vala valse/core/*.vala valse/wala/*.vala --library=out/valse -H out/valse.h -X -fPIC -X -shared -o out/valse.dll

echo " "
echo "--- Valse build ended ---"
echo " "

cp ./out/valse.dll ./test
cp ./out/valse.h ./test
cp ./out/valse.vapi ./test

cd ./test
valac *.vala --vapidir=. --pkg valse --pkg gee-1.0 --pkg libsoup-2.4 -X "-I." -X valse.dll -o serv.exe

echo " "
echo "--- Server is starting ---"
echo " "

try {
    start "http://localhost:8088"
    ./serv
} catch {
    echo "Can't find the server executable. Compilation probably failed"
}

cd ../
