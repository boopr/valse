using Valse;
using Gee;

public class ZdSController : Controller {

    public ZdSController () {
        this.add_action("index", this.index);
        this.add_action("test", this.test);
        this.add_complete_action ("blob", this.blob);
        this.add_complete_action ("message", this.message);
    }

    public Response index () {
        this.db.register<ZdSModel> ();
        ZdSModel model = new ZdSModel ();
        model.greeter = "Plop les zesteux !";
        int rowid = this.db.add (model);

        model.greeter = "Hey, ça va ?";

        this.db.update (rowid, model);

        return page ("zds.wala", model this.db.get_id<ZdSModel> (rowid), new string[] { "yop" });
    }

    public Response test () {
        return page ("D:/code/vala/valse/test.wala");
    }

    public Response blob (Request req, Response res) {
        ZdSModel model = new ZdSModel ();
        model.greeter = req.query ["msg"] + ", ton IP est " + req.ip;
        model.message = "<script>alert('je suis un vilain pirate');</script>";
        model.question = "VOUS M'ENTENDEZ ?";
        model.low = "je déteste les majuscules";
        res.body = page ("zds.wala", model, new string[] {"yop"}).body;
        res.error_code = 201;
        res.headers["X-Powered-By"] = "Valse";
        return res;
    }

    public Response message (Request req, Response res) {
        if (req.method == "POST") {
            ZdSModel model = new ZdSModel ();
            model.greeter = req.form_data ["msg"];
            model.message = req.form_data ["fichier"];
            res.body = page ("msg.wala", model, {}).body;
        } else {
            res.body = page ("test.wala").body;
        }
        return res;
    }

}
